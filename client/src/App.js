import React, { useState, useEffect, useDebugValue } from 'react';
import './index.scss';
import _ from 'lodash';
import Optimizer_SVG from './images/dD_OptimizerEngine.svg'
import Simulator_SVG from './images/dD_RunSimulator.svg'
import {
  Button,
  InputGroupSimpleInput
} from '@crx/react-ui/lib/components';
import { Form, ErrorMessage, Field, Formik, useFormikContext } from 'formik';
import {InputDropdownSimple} from './InputDropdownSimple.js'


// Optimizer: only need specify one value, but can 
// boolean switch on deployed cash, avg spread; only one submitted at a time
// https://ant.design/components/switch/

// issues with available cash as undefined in simulator 


import Highcharts from 'highcharts'
import HighchartsReact from 'highcharts-react-official';

require('highcharts/highcharts-more')(Highcharts)

function CurrencyForm({handleChange_CurrencyForm}) {

    // return (
    //   <div className="CurrencyForm">
    //     <form onSubmit={props.handleSubmit_CurrencyForm}>
    //         <label>
    //         Select Currency  &nbsp;
    //         <select value={props.currency} onChange={props.handleChange_CurrencyForm}>
    //             <option value="EUR">EUR</option>
    //             <option value="USD">USD</option>
    //         </select>
    //         </label>
    //         <input class="submit" type="submit" value="Submit" id="submit" name="submit"/>
    //     </form>
    //   </div>
    // );

    const AutoSubmit = () => {
      const formik = useFormikContext();
      
      React.useEffect(() => {
        // can add logic to not update if not submitted yet 

      formik.submitForm();
      }, [formik.values]);
      
      return null;
    };   

    // const handleChange = async (event) => { // *** where does event come from? the deconstruction defined before the call to onSubmit with enableReinitilize? 
    //         props.handleChange_CurrencyForm(event)
    // }

    return (
      <Formik
        initialValues={{

          simpleDropdown : ""
                  }}     
                  onSubmit={values => { 
                    handleChange_CurrencyForm(values.simpleDropdown)}}
        // update value of props.currency  - TODO 
      >
        {({values}) => (
           // replace with Form  - can compare with DAvid'S code examples 
          <Form>
            {/* {JSON.stringify(values.simpleDropdown)} */}
            <AutoSubmit/>
            <InputGroupSimpleInput  
                          id="currency"
                          name="simpleDropdown"
                          labelText="Select Currency"
                          options={[
                            { key: "", label: '' },
                            { key: "EUR", label: 'EUR' },
                            { key: "USD", label: 'USD' }
                          ]}
                           as={InputDropdownSimple} 
                        />
          </Form>            
        )}
      </Formik>
    )
}

const useCurrencyForm = (callback) => {

  const [currency, setCurrency] = useState('EUR');
  const [currencySelected, setCurrencySelected] = useState(false);

//  function handleChange_CurrencyForm(event) {
//  console.log(event);
//        }
 
  function handleChange_CurrencyForm(value) {
    console.log(value);
    setCurrency(value); // HERE .key
      setCurrencySelected(true);
      var inputs = {contents: null};
      callback(inputs, currency);
  }

  function handleSubmit_CurrencyForm(event) { // TODO - account for no longer using submit button 
      event.preventDefault();
      setCurrencySelected(true);
      var inputs = {contents: null};
      callback(inputs, currency);

      // DO load from db, default data 
  }

  return {
    currency,
    currencySelected,
    handleChange_CurrencyForm,
    handleSubmit_CurrencyForm
  }
}

function SplitPane(props) {
  return (
    <div className="SplitPane">
      <div className="SplitPane-left">
        {props.left}
      </div>
      <div className="SplitPane-right">
        {props.right}
      </div>
    </div>
  );
}

function Simulator(props) {

  function selectSimulator() {
    props.setSimulatorSelected(true);
  }

  if (props.currencySelected && props.simulatorSelected) {
    return (
      <div className= "Simulator_selected" onClick={selectSimulator}>
        <div>
          <div>
            <img src={Simulator_SVG} alt="DD Run Simulator" width= "30%" height="30%"/>
            {/* <h2>DD Run Simulator</h2> */}
            <br></br>
          </div>
        </div>
      <br />
      <form onSubmit={props.handleSubmit_Simulator}>
        <div>
          <label>
            Min. Spread &nbsp; &nbsp; &nbsp; 
          <input
            type="text"
            value={props.inputs_Simulator.minSpread}
            onChange={props.handleInputChange_Simulator}
            name = "minSpread"
            required
          />
          </label>
        </div>
        <div>
        <label>
            Available Cash &nbsp;
          <input
            type="text"
            value={props.inputs_Simulator.availableCash}
            onChange={props.handleInputChange_Simulator}
            name = "availableCash"
            required
          />
          </label>
        </div>
        <br></br>
        <Button type="submit" label='Execute'/>
      </form>
  
      </div>
    )
  } else {
    return (
      <div className= "Simulator" onClick={selectSimulator}>
        <div>
          <div>
            <img src={Simulator_SVG} alt="DD Run Simulator" width= "10%" height="10%"/>
          </div>
        </div>
      </div>
    )
  }
}

function Optimizer(props) {

  function selectOptimizer() {
    props.setSimulatorSelected(false);
  }

  function handleClick_avgSpread() {
    props.setFieldSelection_Optimizer('avg_spread')
  }

  function handleClick_deployedCash() {
    props.setFieldSelection_Optimizer('deployed_cash')
  }

  if (props.currencySelected && !props.simulatorSelected) {
    if (props.fieldSelection_Optimizer == 'avg_spread') {
      return (
        <div className= "Optimizer_selected" onClick={selectOptimizer}>
          <div>
            <div>
              <img src={Optimizer_SVG} alt="DD Run Optimizer" width= "30%" height="30%"/>
                {/* <h2>DD Run Simulator</h2> */}
              <br></br>
            </div>
          </div>
          
          <form onSubmit={props.handleSubmit_Optimizer}>
            <div>
              <label>
                {/* Avg. Spread &nbsp; &nbsp; &nbsp;  */}
                <Button type="button" buttonType='primary' label='Avg. Spread' onClick={handleClick_avgSpread}/> &nbsp;
              <input
                type="text"
                value={props.inputs_Optimizer.avgSpread}
                onChange={props.handleInputChange_Optimizer}
                name = "avgSpread"
              />
              </label>
            </div>
            <div>
              <label>
                {/* Deployed Cash &nbsp; */}
                <Button type="button" buttonType='secondary' label='Deployed Cash' onClick={handleClick_deployedCash}/>
                &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; 
              </label>
            </div>
            <br></br>
            <Button type="submit" label='Execute'/>
          </form>
        </div>
      )
    }
    else if (props.fieldSelection_Optimizer == 'deployed_cash') {
      return (
        <div className= "Optimizer_selected" onClick={selectOptimizer}>
          <div>
            <div>
              <img src={Optimizer_SVG} alt="DD Run Optimizer" width= "30%" height="30%"/>
                {/* <h2>DD Run Simulator</h2> */}
              <br></br>
            </div>
          </div>
          
          <form onSubmit={props.handleSubmit_Optimizer}>
            <div>
              <label>
                {/* Avg. Spread &nbsp; &nbsp; &nbsp;  */}
                <Button type="button" buttonType='secondary'label='Avg. Spread' onClick={handleClick_avgSpread}/>
                &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
              </label>
            </div>
            <div>
              <label>
                {/* Deployed Cash &nbsp; */}
                <Button type="button" buttonType='primary' label='Deployed Cash' onClick={handleClick_deployedCash}/> &nbsp;
              <input
                type="text"
                value={props.inputs_Optimizer.deployedCash}
                onChange={props.handleInputChange_Optimizer}
                name = "deployedCash"
              />
              </label>
            </div>
            <br></br>
            <Button type="submit" label='Execute'/>
          </form>
        </div>
      )
    }
  } else {
    return (
      <div className= "Optimizer" onClick={selectOptimizer}>
        <div>
          <div>
            <img src={Optimizer_SVG} alt="DD Run Optimizer" width= "10%" height="10%"/>
          </div>
        </div>
      </div>
    )
  }
}

function Chart(props) {

  // useEffect(() => {

  //   props.getDataFromDb();

  // }, [JSON.stringify(props.inputs_Simulator.newSubmit)]);
  if (props.currencySelected) {
    return (
      <div>
        <div className= "Chart">
          <div>
            <HighchartsReact
              highcharts={Highcharts}
              options={props.DBdata.chartOptions}
              // callback={}
            />
          </div>
        </div>
      </div>
    )
  } else {
    return (
      <div>
        <div className= "Chart">
        </div>
      </div>
    )
  }
}

function Details(props) {

  if (props.currencySelected) {
    return (
      <div>
        <div className= "Details">
          <div className= "Overview">
            <h3>
              FINANCING OVERVIEW
            </h3>
            <table className="Table_Overview">
              <tr>
                <td>Deployed Cash:</td>
                <td>{props.DBdata.details.overview.deployed_cash + 'k'}</td>
              </tr>
              <tr>
                <td>Available Cash:</td>
                <td>{props.DBdata.details.overview.available_cash + 'k'}</td>
              </tr>
              <tr>
                <td>Discount Earned:</td>
                <td>{props.DBdata.details.overview.discount_earned + 'k'}</td>
              </tr> 
              <tr>
                <td>Invoices Financed:</td>
                <td>{props.DBdata.details.overview.invoices_financed}</td>
              </tr> 
              <tr>
                <td>Suppliers Financed:</td>
                <td>{props.DBdata.details.overview.invoices_financed}</td>
              </tr> 
              <tr>
                <td>Min. Spread:</td>
                <td>{props.DBdata.details.overview.min_spread + ' bps'}</td>
              </tr> 
              <tr>
                <td>Avg. Spread:</td>
                <td>{props.DBdata.details.overview.avg_spread + ' bps'}</td>
              </tr> 
              <tr>
                <td>Max. Spread:</td>
                <td>{props.DBdata.details.overview.max_spread + ' bps'}</td>
              </tr>    
            </table>
          </div>
          <br></br>
          <div className= "Opportunity">
            <h3>
              REMAINING OPPORTUNITY
            </h3>
            <table className="Table_Opportunity">
              <tr>
                <td>Open Amount:</td>
                <td>{props.DBdata.details.opportunity.open_amount + 'k'}</td>
              </tr>
              <tr>
                <td>Invoices Open:</td>
                <td>{props.DBdata.details.opportunity.invoices_open}</td>
              </tr>
              <tr>
                <td>Suppliers Open:</td>
                <td>{props.DBdata.details.opportunity.suppliers_open}</td>
              </tr> 
              <tr>
                <td>Min. Spread:</td>
                <td>{props.DBdata.details.opportunity.min_spread + ' bps'}</td>
              </tr> 
              <tr>
                <td>Avg. Spread:</td>
                <td>{props.DBdata.details.opportunity.avg_spread + ' bps'}</td>
              </tr> 
              <tr>
                <td>Max. Spread:</td>
                <td>{props.DBdata.details.opportunity.max_spread + ' bps'}</td>
              </tr>   
            </table>
          </div>
        </div>
      </div>
    )
  } else {
    return (
      <div>
        <div className= "Details">
        </div>
      </div>
    )
  }
}

const useDBdata = () => {

  const [DBdata, setDBdata] = useState({
    chartOptions: {
      chart: {
          type: 'columnrange',
          inverted: true
      },

      accessibility: {
          description: 'Image description: A column range chart compares the monthly temperature variations throughout 2017 in Vik I Sogn, Norway. The chart is interactive and displays the temperature range for each month when hovering over the data. The temperature is measured in degrees Celsius on the X-axis and the months are plotted on the Y-axis. The lowest temperature is recorded in March at minus 10.2 Celsius. The lowest range of temperatures is found in December ranging from a low of minus 9 to a high of 8.6 Celsius. The highest temperature is found in July at 26.2 Celsius. July also has the highest range of temperatures from 6 to 26.2 Celsius. The broadest range of temperatures is found in May ranging from a low of minus 0.6 to a high of 23.1 Celsius.'
      },

      title: {
          text: ''
      },

      subtitle: {
          text: ''
      },

      annotations: [
            {
              labelOptions: {
                verticalAlign: 'top',
                x: 55
              }
            },
            {
                labels: [{
                point: { x: .25, y: .25, xAxis: 0, yAxis: 0},
                text: 'Min. Spread: 25 bps'
              }]
            },
            {
              shapes: [
                {
                  point: { x: .25, y:700, xAxis: 0, yAxis: 0 },
                  type: 'rect',
                  width: 2,
                  height: 20
                }
              ]
            },
            {
              shapes: [
                {
                  point: { x: 0.25, y:690, xAxis: 0, yAxis: 0},
                  type: 'rect',
                  width: 2,
                  height: 20,
                  color: '#E1523D'
                }
              ]
            }
      ],

      xAxis: {
          gridLineWidth: 0,

          // min: 0,
          reversed:false,
          title: {
              text: 'Financing Rate',
          },
          labels: {
              format: '{value} bps'
          },
      },

      yAxis: {
          gridLineWidth: 0,
          min:0,
          title: {
              text: 'Volume',
          },
          labels: {
            format: '{value}k'
        },
      },

      tooltip: {
          valueSuffix: '€',

          formatter: function() {
            var output = '';
            if (this.point.name == 'Financed' || this.point.name == 'Unfinanced') {
              output = 'bps: <b>' + this.x + '<br>' + this.point.info;
              console.log(output);
            }
            else if (this.point.name == 'Min_spread') {
              output = 'Min Spread: ' + this.x + ' bps';
              console.log(output);
            }
            else if (this.point.name == 'Cash_Deployed') {
              output = 'Cash Deployed: ' + this.point.high;
              console.log(output);
            }
            else if (this.point.name == 'Cash_Available') {
              output = 'Cash Available: ' + this.point.high;
              console.log(output);
            }
            return output;
          }
      },

      plotOptions: {
          columnrange: {
              grouping: false,
              dataLabels: {
                  enabled: false,
                  format: '{x}'
              }
          },
          series: {
            pointWidth: 15
          },
          scatter: {
              marker: {
                  radius: 5,
                  states: {
                      hover: {
                          enabled: true,
                          lineColor: 'rgb(100,100,100)'
                      }
                  }
              },
              states: {
                  hover: {
                      marker: {
                          enabled: false
                      }
                  }
              },
              tooltip: {
                  headerFormat: '<b>{series.name}</b><br>',
                  pointFormat: '{point.x} cm, {point.y} kg'
              }
          }
      },

      legend: {
          enabled: false
      },

      series: [
        {
          name:'bid_bins',
          color:'#795548',
          data: [
            {
              x:400,
              low:0,
              high:50,
              name:'Unfinanced',
              color: '#EBD2CE',
              // info: 'BMW: 40 <br> GOA: 10',
              info: 'Volume: 50k'
            },
            {
              x:325,
              low:50,
              high:200,
              name:'Unfinanced',
              color: '#EBD2CE',
              // info: 'ABC: 75 <br> DEF: 75',
              info: 'Volume: 150k'
            },
            {
              x:250,
              low:200,
              high:900,
              name:'Unfinanced',
              color: '#EBD2CE',
              // info: 'AML: 200 <br> KYC: 200 <br> REG: 50 <br> ULATION: 50',
              info: 'Volume: 700k'
            },
            {
              x:150,
              low:900,
              high:1400,
              name:'Unfinanced',
              color: '#EBD2CE',
              // info: 'XYZ: 500',
              info: 'Volume: 500k'
            }
          ]
        }
      ]
    },

    details: {
      overview: {
        available_cash: 0,
        deployed_cash: 0,
        discount_earned: 0,
        invoices_financed: 0, 
        suppliers_financed: 0,
        min_spread: 0,
        avg_spread: 0,
        max_spread: 0
      },
      opportunity: {
        open_amount: 0,
        invoices_open: 0,
        suppliers_open: 0,
        min_spread: 0,
        avg_spread: 0,
        max_spread: 0
      }
    }
  });

  const getDataFromDB = (inputs, currency) => {
    
    var inputs_keys = Object.keys(inputs);
    var isSimulatorInput = inputs_keys.includes("minSpread") && inputs_keys.includes("availableCash");
    var isOptimizerInput = inputs_keys.includes("deployedCash") || inputs_keys.includes("avgSpread");

    console.log('getDataFromDB');
    if (isSimulatorInput) {
      fetch('http://localhost:3001/api/getData')
      .then((response) => {
        return response.json();
      })
      .then((json_response) => {
  
        let foundResponse = null;
        
        console.log('isSimulatorInput: looking for data');
        for (let i = 0; i < json_response.data.length; i++) {
  
          if((Number(inputs.minSpread) == json_response.data[i].min_spread) && (Number(inputs.availableCash) == json_response.data[i].available_cash) &&  (!json_response.data[i].before_tool_execution)) { // (currency == json_response.data[i].currency) &&
            foundResponse = json_response.data[i];
          }
        }
  
        if (foundResponse) {
  
          var newChartOptions = {...DBdata.chartOptions}
          newChartOptions.series = foundResponse.series;
          console.log('foundResponse');
          setDBdata({
            ...DBdata,
            chartOptions: newChartOptions,
            details: foundResponse.details
          });
        }
      })
    } else if (isOptimizerInput) { // Optimizer input 
      fetch('http://localhost:3001/api/getData')
      .then((response) => {
        return response.json();
      })
      .then((json_response) => {
  
        let foundResponse = null;
  
        for (let i = 0; i < json_response.data.length; i++) {
  
          if(((Number(inputs.deployedCash) == json_response.data[i].deployed_cash) || (Number(inputs.avgSpread) == json_response.data[i].avg_spread)) && (!json_response.data[i].before_tool_execution)) { // (currency == json_response.data[i].currency) &&
            foundResponse = json_response.data[i];
          }
        }
  
        if (foundResponse) {
  
          var newChartOptions = {...DBdata.chartOptions}
          newChartOptions.series = foundResponse.series;
  
          setDBdata({
            ...DBdata,
            chartOptions: newChartOptions,
            details: foundResponse.details
          });
        }
      })
    } else if (inputs.contents == null) { // case of generating chart and details when currency is selected 
        fetch('http://localhost:3001/api/getData')
        .then((response) => {
          return response.json();
        })
        .then((json_response) => {
    
          let foundResponse = null;
    
          for (let i = 0; i < json_response.data.length; i++) {
    
            if((currency == json_response.data[i].currency) && (json_response.data[i].before_tool_execution)) {
              foundResponse = json_response.data[i];
            }
          }
    
          if (foundResponse) {
    
            var newChartOptions = {...DBdata.chartOptions}
            newChartOptions.series = foundResponse.series;
    
            setDBdata({
              ...DBdata,
              chartOptions: newChartOptions,
              details: foundResponse.details
            });
          }
        })
    } else {
      // error 
    }
  };

  return {
    DBdata, 
    getDataFromDB
  };
}

const useSimulatorForm = (callback, currency) => {

  const [inputs_Simulator, setinputs_Simulator] = useState({});

  const handleSubmit_Simulator = (event) => { // manage submit event. prevent default behavior of browser (refresh page)
    if (event) {
      event.preventDefault();
    }
    console.log('handleSubmitSimulator');
    callback(inputs_Simulator, currency);
  }

  const handleInputChange_Simulator = (event) => { // manage event where user gives input
      event.persist();
      setinputs_Simulator(inputs_Simulator => ({...inputs_Simulator, [event.target.name]:
        event.target.value}));
  }

  return {
    handleSubmit_Simulator,
    handleInputChange_Simulator,
    inputs_Simulator
  };
}

const useOptimizerForm = (callback, currency) => {

  const [inputs_Optimizer, setinputs_Optimizer] = useState({});

  const handleSubmit_Optimizer = (event) => { // manage submit event. prevent default behavior of browser (refresh page)
    if (event) {
      event.preventDefault();
    }
    callback(inputs_Optimizer, currency);
  }

  const handleInputChange_Optimizer = (event) => { // manage event where user gives input
      event.persist();
      setinputs_Optimizer(inputs_Optimizer => ({...inputs_Optimizer, [event.target.name]:
        event.target.value}));
  }

  return {
    handleSubmit_Optimizer,
    handleInputChange_Optimizer,
    inputs_Optimizer
  };
}

function App(props) {

  // READ: https://medium.com/trabe/passing-callbacks-down-with-react-hooks-4723c4652aff

  const {DBdata, getDataFromDB} = useDBdata();

  const {currency, currencySelected, handleChange_CurrencyForm, handleSubmit_CurrencyForm} = useCurrencyForm(getDataFromDB);

  const {inputs_Simulator, handleInputChange_Simulator, handleSubmit_Simulator} = useSimulatorForm(getDataFromDB, currency);

  const {inputs_Optimizer, handleInputChange_Optimizer, handleSubmit_Optimizer} = useOptimizerForm(getDataFromDB, currency);

  const [simulatorSelected, setSimulatorSelected] = useState(true); 

  const [fieldSelection_Optimizer, setFieldSelection_Optimizer] = useState('avg_spread')

  return (
          <div >
              <span className="title">
                <h1 className="Title">Dynamic Discounting Tool &nbsp;</h1>
              </span>     
              {/* <div className="Explanation">This tool ... </div> */}
              <div>
              <CurrencyForm currency={currency} handleChange_CurrencyForm={handleChange_CurrencyForm} handleSubmit_CurrencyForm={handleSubmit_CurrencyForm} />
              </div>
              <div>
              <SplitPane
              left={
                <Simulator currency={currency} currencySelected={currencySelected} inputs_Simulator={inputs_Simulator} handleInputChange_Simulator={handleInputChange_Simulator} handleSubmit_Simulator={handleSubmit_Simulator} simulatorSelected={simulatorSelected} setSimulatorSelected={setSimulatorSelected}/>
              }
              right={
                <Optimizer currency={currency} currencySelected={currencySelected} simulatorSelected={simulatorSelected} setSimulatorSelected={setSimulatorSelected} inputs_Optimizer={inputs_Optimizer} handleInputChange_Optimizer={handleInputChange_Optimizer} handleSubmit_Optimizer={handleSubmit_Optimizer} fieldSelection_Optimizer={fieldSelection_Optimizer} fieldSelection_Optimizer={fieldSelection_Optimizer} setFieldSelection_Optimizer={setFieldSelection_Optimizer}/>
              } />
              </div>
              <div>
              <SplitPane
                left={
                <Chart DBdata={DBdata} currencySelected={currencySelected}/>
                  // <div>BLAH</div> 
                }
                right={
                  <Details currencySelected={currencySelected} DBdata={DBdata}/>
                } />
              </div>
          </div>
      );
}

export default App;

// graph generation on currency submission 

// http://infradocker01.office.crxmarkets.com:8880/?path=/story/styleguide--9-navigation
// 

// details board - next two days 
// selectively activate optimizer or simulator 
// working optimizer 