import * as PropTypes from 'prop-types';
import React from 'react';
import { Dropdown } from 'primereact/dropdown';
import { Field, useFormikContext } from 'formik';
import nestedProperty from 'nested-property';
import css from './input-dropdown-simple.module.scss';
const scrollHeightSizeConfig = {
  M: '400px',
};
export function InputDropdownSimple({ id, name, options, dataKey, scrollHeightSize, optionLabel, ...rest }) {
  const { values, setFieldValue } = useFormikContext();
  const currentValue = nestedProperty.get(values, name);
  const scrollHeight = scrollHeightSize ? scrollHeightSizeConfig[scrollHeightSize] : scrollHeightSize;
  return (
    <Field
      {...{ id, name, options }}
      className={css.container}
      as={Dropdown}
      dataKey={dataKey}
      optionLabel={optionLabel}
      filter
      filterBy={optionLabel}
      scrollHeight={scrollHeight}
      onChange={({ value }) => {
        setFieldValue(name, value[dataKey]);
      }}
      {...rest}
      value={options.find(option => option[dataKey] && option[dataKey] === currentValue)}
    />
  );
}
InputDropdownSimple.defaultProps = {
  scrollHeightSize: null,
  dataKey: 'key',
  optionLabel: 'label',
};
InputDropdownSimple.propTypes = {
  id: PropTypes.string.isRequired,
  name: PropTypes.string.isRequired,
  options: PropTypes.arrayOf(PropTypes.object).isRequired,
  scrollHeightSize: PropTypes.oneOf(['M']),
  dataKey: PropTypes.string,
  optionLabel: PropTypes.string,
};
