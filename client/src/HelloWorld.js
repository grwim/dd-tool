import React, { useState, useEffect, useDebugValue } from 'react';
import './index.scss';
import _ from 'lodash';
import Optimizer_SVG from './images/dD_OptimizerEngine.svg'
import Simulator_SVG from './images/dD_RunSimulator.svg'
import {
  Button,
  InputGroupSimpleInput
} from '@crx/react-ui/lib/components';
import { Form, ErrorMessage, Field, Formik, useFormikContext } from 'formik';
import {InputDropdownSimple} from './InputDropdownSimple.js';


const AutoSubmit = () => {
    const formik = useFormikContext();
    
    React.useEffect(() => {
      // can add logic to not update if not submitted yet 

    formik.submitForm();
    }, [formik.values]);
    
    return null;
  };   


function SimpleFormik() {

    return (
      <Formik initialValues={{
          simpleDropdown : ""
        }}
      >
        {({values}) => (
          <Form>
            {JSON.stringify(values)}
            {/* <AutoSubmit/> */}
            <InputGroupSimpleInput  
                          id="currency"
                          name="simpleDropdown"
                          labelText="Select Currency"
                          options={[
                            { key: "", label: '' },
                            { key: "EUR", label: 'EUR' },
                            { key: "USD", label: 'USD' }
                          ]}
                           as={InputDropdownSimple} 
                        />
          </Form>            
        )}
      </Formik>
    )
}


function HelloWorld(props) {
  
    return (
            <div >
                <SimpleFormik/>
            </div>
        );
  }
  
export default HelloWorld;
  